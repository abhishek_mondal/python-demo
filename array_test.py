import array

ar = array.array('i', [1, 2, 3, 4, 5, 6])
print(ar)

import array as arr

ar = arr.array('i', [1, 2, 3, 4, 5, 6])
print(ar)

from array import *

ar = array('i', [1, 2, 3, 4, 5, 6])
print(ar)
print(ar[3])
print(ar[-2])

# Array Operations
print('Length of the array is :', len(ar))

# Adding element to an array
ar.append(7)
print(ar)
ar.extend([8, 9, 10, 2])
print(ar)
ar.insert(0, 11)
print(ar)

# pop and remove
ar.pop()
print(ar)
ar.pop(5)
print(ar)
ar.pop(-1)
ar.remove(11)
print(ar)

# array concatenation
ab = array('i', [10, 20, 30, 40])
ac = array('i')
ac = ar + ab
print(ac)

# Slicing
print(ac[2:7])
print(ac[2:-2])
# reversing - not preferred as it exhausts the memory
print(ac[::-1])

# Looping through array
for a in ac:
    print(a)

for a in ac[0:-5]:
    print(a)

temp = 0
# while temp < ac[5]:
while temp < len(ac):
    print("Through While : ", ac[temp])
    temp += 1
