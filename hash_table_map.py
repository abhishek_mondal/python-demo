my_dict = {1: 'Python', 2: 'Java', 3: 'Go'}
print(my_dict)
print(type(my_dict))

new_dict = dict(Pyhton=1, Java=2, Go=3)
print(new_dict)
print(type(new_dict))

# nested dictionary
emp_details = {'Employee': {'Abhi': {'ID': '001', 'Designation': 'AST'},
                            'Payel': {'ID': '002', 'Designation': 'Tech Lead'}}}
print(emp_details)

# Hash Table operations
print(new_dict['Pyhton'])
print(my_dict[1])
print(my_dict.keys())
print(my_dict.values())
print(my_dict.get(3))
for item in my_dict:
    print(item)
for item in my_dict.values():
    print(item)
for x, y in my_dict.items():
    print(x, y)

# update operation
my_dict[1] = 'Scala'
my_dict[4] = 'Groovy'
print(my_dict)

# delete items
my_dict.pop(1)
print(my_dict)
my_dict.popitem()
print(my_dict)
del my_dict[2]
print(my_dict)

# DataFrame is a 2 dimensional dta structure that consists of columns of various types
import pandas as pd
df = pd.DataFrame(emp_details['Employee'])
print(df)
