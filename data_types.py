"""
Data Types are following types -
1. Numbers
2. String
3. List
4. Dictionary
5. Tuple
6. Set
"""
# Numbers
x = 10
print(type(x))
x = 10.25
print(type(x))
x = 10j
print(type(x))
x = True
print(type(x))

# String Data Types
name = "Abhishek"
print(len(name))
print(name[2])
print(name[2:7])
print(name.upper())
print(name.replace('h', 'i'))

# List - a ordered mutable collection
myList = [1, 2, 3, 'Abhi', 'Python']
print(myList)
print(myList[2:6])
myList[2] = 6
print(myList)
myList.append(4)
print(myList)
myList.insert(4, 'Suraj')
print(myList)
print(myList.reverse())

# Dictionary - just like Map in java
courses = {1: 'Java',
           2: 'Groovy',
           3: 'Python',
           1: 'JavaScript',
           'fourth': 'Scala'}
print(courses)
print(courses['fourth'])
print(courses.get(2))
courses[2] = 'Go Lang'
print(courses)

# Tuple - Ordered, Can't be changed(immutable), Duplicate entries are present
colors = [10, 20, 10, 'Red', 'White', 'Orange', 'Pink', 'Red']
print(colors)
print(colors[3])
print(colors[3:6])
print(colors.count('Red'))

# Set - Unordered, no duplicates
mySet = {10, 20, 30, 40, 10, 20, 'Red', 'Green'}
print(mySet)

# Range
print(list(range(1, 10)))

# Type conversion
str1 = 'Abhi'
i = 10
print(str1 + str(i))
a = {1, 2, 3, 2}
b = list(a)
print(b)
