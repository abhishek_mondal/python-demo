# List is a ordered collection of objects which can contains heterogeneous data
# and it's a mutable data structure
prices = []
print(prices)
car_details = ['KIA', 'TOYOTA', 'TATA', 'SKODA']
temps = [32.0, 78.0, 2.0, 8, 'Abhi']
print(temps)
for item in temps:
    print(item)
everything = [prices, temps, car_details]
print(everything)
vowels = ['a', 'e', 'i', 'o', 'u']
word = 'Milliways'
for letter in word:
    if letter in vowels:
        print(letter)

print('Prices List length before:', len(prices))
prices.append(10)
prices.append(28)
prices.append(20)
prices.insert(2,12)
print('Prices List length after:', len(prices))
if 15 not in prices:
    prices.append(15)
# Remove objects from list
nums = [1,2,3,4,5]
nums.remove(3)
print(nums)
nums.pop(3)
print(nums)
help(list)