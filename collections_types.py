"""
There are four collections -
List, Tuples, Set, Dictionary
"""

"""
Specialized Collection Data Types -
namedtuple(), Chainmap, deque, Counter, defaultdict, OrderdDict,
UserDict, UserList, UserString
"""

# namedtuple() returns a tuple with a named value for each element in the tuple
from collections import namedtuple

names = namedtuple('courses', 'name, technology')
temp = names('Python', 'ML')
print(temp)
s = names._make(['Python', 'AI'])
print(s)

# deque - pronounced as 'deck' is an optimized list to perform insertion and deletion easily
from collections import deque

myList = ['a', 'b', 'h', 'i', 's', 'h', 'e', 'k']
d = deque(myList)
print(d)
d.append('python')
print(d)
d.appendleft('java')
print(d)
d.pop()
print(d)
d.popleft()
print(d)

# Chainmap is a Dictionary like class for creating a single view of multiple mappings.
from collections import ChainMap

a = {1: 'Python', 2: 'Java'}
b = {3: 'Go', 4: 'Groovy'}
resultMap = ChainMap(a, b)
print(resultMap)

# Counter is a Dictionary subclass for counting hashable objects
from collections import Counter

a = [1, 2, 3, 4, 5, 6, 2, 4, 2, 2, 6, 8, 3, 1, 5, 1, 8, 9]
c = Counter(a)
print(c)
print(list(c.elements()))
print(c.most_common())
sub = {2: 1, 1: 2}
c.subtract(sub)
print(c)

# OrderedDict is a Dictionary subclass which remembers the ordered in which the entries were done
from collections import OrderedDict

od = OrderedDict()
od[1] = 'A'
od[2] = 'b'
od[3] = 'h'
od[4] = 'i'
od[5] = 's'
od[6] = 'h'
od[7] = 'e'
od[8] = 'k'

print(od)
print(od.keys())
print(od.values())
od[1] = 'P'
print(od)

# defaultdict is a Dictionary subclass which calls a factory function to supply missing values
from collections import defaultdict

df = defaultdict(int)
df[1] = 'Python'
df[2] = 'Java'
print(df)
print(df[3])

# UserDict is a wrapper around Dictionary objects for easier dictionary sub-classing
from collections import UserDict

d = {1: 'Java', 2: 'Python', 3: 'Go'}
ud = UserDict(d)
print(ud)

# UserList is a wrapper around list objects for easier List sub-classing
from collections import UserList

l = [1, 2, 3, 4, 5, 6]
ul = UserList(l)
print(ul)

# UserString is a wrapper around string objects for easier String sub-classing
from collections import UserString

name = 'Abhishek'
us = UserString(name)
print(us)